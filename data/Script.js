//spesial//
const spesial = () => {
    fetch('/data/spesial.json')
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(data => {
            console.log(data)
            let cardspesial = "";
            data.forEach(menu => {
                cardspesial += `<div class="col mb-4">
                                    <div class="card  card-spesial">
                                        <img src="${menu.gambar}" class="card-img-top" alt="Poster 1">
                                    <div class="card-body">
                                        <h5 class="card-title">${menu.nama}</h5>
                                        <a class="btn btn-danger">${menu.harga}</a>
                                    </div>
                                    </div>
                                </div>`;
            });

            const spesial = document.querySelector("#spesial");
            spesial.innerHTML = cardspesial;

        }).catch(error => {
            console.error(error);
        });
};
spesial();

//combo//
const combo = () => {
    fetch('/data/combo.json')
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(data => {
            console.log(data)
            let cardcombo = "";
            data.forEach(menu => {
                cardcombo += `<div class="col mb-4">
                                    <div class="card  card-combo">
                                        <img src="${menu.gambar}" class="card-img-top" alt="Poster 1">
                                    <div class="card-body">
                                        <h5 class="card-title">${menu.nama}</h5>
                                        <a class="btn btn-danger">${menu.harga}</a>
                                    </div>
                                    </div>
                                </div>`;
            });

            const combo = document.querySelector("#combo");
            combo.innerHTML = cardcombo;

        }).catch(error => {
            console.error(error);
        });
};
combo();

//coffe//
const coffe = () => {
    fetch('/data/coffe.json')
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(data => {
            console.log(data)
            let cardcoffe = "";
            data.forEach(menu => {
                cardcoffe += `<div class="col mb-4">
                                    <div class="card  card-coffe">
                                        <img src="${menu.gambar}" class="card-img-top" alt="Poster 1">
                                    <div class="card-body">
                                        <h5 class="card-title">${menu.nama}</h5>
                                        <a class="btn btn-danger">${menu.harga}</a>
                                    </div>
                                    </div>
                                </div>`;
            });

            const coffe = document.querySelector("#coffe");
            coffe.innerHTML = cardcoffe;

        }).catch(error => {
            console.error(error);
        });
};
coffe();